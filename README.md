# Project for challenge 3 of 305cde (Developing the modern Web 2)

## Overview
The core of this project is the shopping basket module. It can be included to provide e-shopping functionality. Sample usage of the module is demonstrated.

## How it works
The URL entered should point to a firebase backend where the JSON objects _ShoppingItems_ and optionally _BasketContent_ are present. Sample _ShoppingItems_ which could be imported in another firebase are provided in _items.json_ in this repository. Switching between databases is as easy as typing in the host-address of the new database and clicking the _Refresh_-button.

The main application is contained in a _Basket_-module which attempts to retrieve the data using its _Firebase_-module using AJAX with Promises as used during the corresponding worksheet. Items can be selected from the shopping list and added to the basket. Current basket state is displayed at the top, where a button can be used to inspect the basket.

The amount of items in the basket may be adjusted. All important events are being recorded using the _Logger_-module.

## Reflection
Apart from creating the modules required by the challenge description, I created a _Logger_-module since that is a useful piece of code to re-use, which is demonstrated in challenge 9 (guestbook). The main reason for creating the logger is to facilitate switching between a _debug_ and a _deploy_ setup since I want alerts disabled during debugging since I check the console but enabled once deployed since the user should not be required to check the console in order to understand what is going on.

The main reason for using firebase is to persist data remotely. Since no complex database is required, firebase is a good option. Furthermore, using promises gave me a chance to practice working with asynchronous calls in JavaScript.

One thing that could certainly be improved is that persisting changes to the amount of a item in the basked could be more fine-grained, updating simply the number, rather than the whole _BasketContent_.

Database access in the _Firebase_-module is split internally into _get_ and _put_. The accessible methods like _getShoppingItems()_ simply call _get_/_set_ with the correct URL and return a promise which will contain the parsed JSON data from the call if successful. It is important that the _Basket_-module does not have knowledge of the URLs because it should not be concerned with the database access, it is only concerned with processing the data that has been retrieved.

The _Basket_-module contains local copies of the data and provides access to information related to the data to whichever piece of code uses the _Basket_-module. In this case, it is the main code within shopping.js. That is primarily concerned with handling user interaction and displaying relevant data in the HTML.