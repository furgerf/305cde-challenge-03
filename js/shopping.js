/*global app*/
var shopping = (function () {
    'use strict';

    var items = [],
        logger,
        basket,
        firebase,
        updateBasketElements = (function () {
            // get references to the HTML-elements that are being modified
            var basketCount = document.getElementById("basketCount"),
                basketDistinctCount = document.getElementById("basketDistinctCount"),
                basketCost = document.getElementById("basketCost");

            return function () {
                // update basket count and cost
                basketDistinctCount.innerHTML = 'Currently <span class="highlight">' + (basket ? basket.getItemCount() : 0) + '</span> different item' + (basket ? (basket.getItemCount() === 1 ? "" : "s") : "s") + ' in basket.';
                basketCount.innerHTML = 'Total items in basket: <span class="highlight">' + (basket ? basket.getTotalItemsCount() : 0) + '</span>.';
                basketCost.innerHTML = 'The current items cost <span class="highlight">£' + (basket ? basket.getTotalCost() : 0) + '</span>.';
            };
        }()),
        showTable = (function () {
            // get a reference to the HTML table
            var table = document.getElementById("table");

            function getTableHeader() {
                // return HTML-code to display the header row for the table
                return '<td width="150" class="header">Item Title</td><td width="400" class="header">Item Description</td><td width="100" class="header">Price</td><td class="header">Amount</td><td class="header"></td>';
            }

            function getShoppingTableRow(item) {
                // create HTML-code for button and numeric input
                var buttonCode = "<button onclick=\"shopping.addToBasket(" + item.id + ")\">Add to basket!</button>",
                    numericCode = "<input type=\"number\" value=\"1\" min=\"0\" id=\"" + item.id + "\">";

                // return HTML-code to display a row in the table for the parameter item
                return "<td>" + item.title + "</td><td>" + item.description + "</td><td>£" + item.price + "</td><td>" + numericCode + "</td><td>" + buttonCode + "</td>";
            }

            function getBasketTableRow(item) {
                // create HTML-code for button and numeric input
                var buttonCode = "<button onclick=\"shopping.updateAmount(" + item.item.id + ")\">Update</button>",
                    numericCode = "<input type=\"number\" value=\"" + item.amount + "\" min=\"0\" id=\"" + item.item.id + "\">";

                // return HTML-code to display a row in the table for the parameter item
                return "<td>" + item.item.title + "</td><td>" + item.item.description + "</td><td>£" + item.item.price + "</td><td>" + numericCode + "</td><td>" + buttonCode + "</td>";
            }

            return {
                shoppingTable: function () {
                    // update the table's html
                    table.innerHTML = "<tr>" + getTableHeader();
                    items.forEach(function (item) {
                        table.innerHTML += getShoppingTableRow(item);
                    });
                    table.innerHTML += "</tr>";
                },
                basketTable: function () {
                    // update the table's html
                    table.innerHTML = "<tr>" + getTableHeader();
                    basket.getBasketItems().forEach(function (item) {
                        table.innerHTML += getBasketTableRow(item);
                    });
                    table.innerHTML += "</tr>";
                }
            };
        }()),
        addToBasket = function (id) {
            // prepare variables
            var num = document.getElementById(id),
                amount = parseInt(num.value, 10);


            // don't do anything if we're not adding any objects
            if (amount === 0) {
                return;
            }

            // ensure amount is valid
            if (!amount) {
                logger.logAlert("Please enter a valid amount");
                return;
            }

            // update basket + notify user
            basket.addItem(id, amount);
            logger.logAlert("Added " + amount + " instances of " + items[parseInt(id, 10)].title);

            // reset numeric input value to 1
            num.value = 1;

            // update the info display on the current basket details
            updateBasketElements();
        },
        updateAmount = function (id) {
            // prepare variables
            var num = document.getElementById(id),
                newAmount = parseInt(num.value, 10),
                currentAmount = basket.getAmountInBasket(id);

            // dont do anything if amount hasnt been changed
            if (newAmount === currentAmount) {
                return;
            }

            // only proceed if amount is valid
            if (!newAmount && newAmount !== 0) {
                logger.logAlert("Please enter a valid amount");
                return;
            }

            // add or remove?
            if (newAmount > currentAmount) {
                basket.addItem(id, newAmount - currentAmount);
            } else {
                basket.removeItem(id, currentAmount - newAmount);
            }

            // notify user and reload basket
            logger.logAlert((newAmount > currentAmount ? "Increased" : "Decreased") + " amount of " + items[parseInt(id, 10)].title + " from " + currentAmount + " to " + newAmount);

            if (newAmount === 0) {
                // if count is zero, remove row (redraw table)
                showTable.basketTable();
            }

            // update the info display on the current basket details
            updateBasketElements();
        },
        toggleBetweenShoppingAndBasket = (function () {
            // prepare references
            var showingShoppingTable = true,
                button = document.getElementById("toggleBasket");

            return {
                toggle: function () {
                    // toggle state flag
                    showingShoppingTable = !showingShoppingTable;

                    // show proper text on button
                    button.value = showingShoppingTable ? "Show Basket" : "Show Shopping List";

                    // show proper table
                    showTable[showingShoppingTable ? "shoppingTable" : "basketTable"]();
                },
                set: function (shoppingTable) {
                    // set state flag
                    showingShoppingTable = shoppingTable;

                    // show proper text on button
                    button.value = showingShoppingTable ? "Show Basket" : "Show Shopping List";

                    // show proper table
                    showTable[showingShoppingTable ? "shoppingTable" : "basketTable"]();
                }
            };
        }());

    // add button event listeners
    document.getElementById('toggleBasket').addEventListener("click", toggleBetweenShoppingAndBasket.toggle);

    return {
        // initialize shopping with logger and firebase instance
        init: function (log, fb) {
            logger = log;
            firebase = fb;

            // load shopping items
            items = [];
            firebase.getShoppingItems().then(function (data) {
                data = data || [];
                data.forEach(function (item) {
                    items.push(new app.Item(item.title, item.description, item.price, item.id, logger));
                });

                // finally, load shopping basket
                basket = new app.Basket(firebase, logger, items, updateBasketElements);

                // display the items
                toggleBetweenShoppingAndBasket.set(true);
            }).catch(function (err) {
                // notify the user of an error
                logger.logAlert(err);

                // the basket may still have loaded the old data, fix that
                basket = null;

                // update view
                updateBasketElements();
                showTable.shoppingTable();
            });
        },
        // global access to button.onclick methods
        addToBasket: addToBasket,
        updateAmount: updateAmount,
        // global access for re-initialization
        getLogger: function () {
            return logger;
        }
    };
}());

////////////////////
// EVENT HANDLING //
////////////////////
function refreshData() {
    'use strict';

    var url = document.getElementById("firebaseUrl").value;

    shopping.getLogger().logAlert('Reloading data from "' + url + "'");
    shopping.init(shopping.getLogger(), new app.Firebase(url, shopping.getLogger()));
}

////////////////////
// INITIALIZATION //
////////////////////
function init() {
    'use strict';

    // create Important Objects
    // must do that once the window has loaded
    // for app to be loaded properly
    var logger = new app.Logger(0xFF),
        firebase = new app.Firebase(document.getElementById("firebaseUrl").value, logger);

    // tell shopping about Important Objects
    shopping.init(logger, firebase);
}

window.onload = init;