/*global Promise,alert,app*/

(function (window) {
    'use strict';

    function Firebase(baseUrl, existingLogger) {
        // logger instance
        var logger = typeof existingLogger === 'object' ? existingLogger : new app.Logger(0xFF),
            // AJAX methods
            get = function (url) {
                return new Promise(function (resolve, reject) {
                    // Do the usual XHR stuff
                    var req = new XMLHttpRequest();
                    req.open('get', url);

                    // handle response
                    req.onload = function () {
                        // check status
                        if (req.status === 200) {
                            // Resolve the promise with the response text
                            resolve(req.response);
                        } else {
                            // Otherwise reject with the status text
                            reject(new Error(req.statusText));
                        }
                    };

                    // Handle network errors
                    req.onerror = function () {
                        reject(new Error("Network Error"));
                    };

                    // Make the request
                    req.send();
                });
            },
            put = function (url, data) {
                return new Promise(function (resolve, reject) {
                    // Do the usual XHR stuff
                    var req = new XMLHttpRequest();
                    req.open('put', url);

                    // handle response
                    req.onload = function () {
                        // check status
                        if (req.status === 200) {
                            // Resolve the promise with the response text
                            resolve(req.response);
                        } else {
                            // Otherwise reject with the status text
                            reject(new Error(req.statusText));
                        }
                    };

                    // Handle network errors
                    req.onerror = function () {
                        reject(new Error("Network Error"));
                    };

                    // Make the request
                    req.send(data);
                });
            };

        if (typeof baseUrl !== 'string') {
            logger.logAlert("No or invalid base URL address provided!");
        }
        
        // public access: specific tasks
        this.getShoppingItems = function () {
            logger.logConsole("GETting shopping items");
            return get(baseUrl + "ShoppingItems.json").then(JSON.parse);
        };

        this.getBasketContent = function () {
            logger.logConsole("GETting basket content");
            return get(baseUrl + "BasketContent.json").then(JSON.parse);
        };

        this.setBasketContent = function (data) {
            logger.logConsole("PUTting data: " + data);
            return put(baseUrl + "BasketContent.json", data);
        };
    }

    // Export to window
    window.app = window.app || {};
    window.app.Firebase = Firebase;
}(window));