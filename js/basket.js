/*global app*/

(function (window) {
    'use strict';

    function Basket(fb, existingLogger, shoppingItems, onDataLoaded) {
        // internal variables (state)
        var logger = typeof existingLogger === 'object' ? existingLogger : new app.Logger(0xFF),
            firebase = fb,
            basketItems = [];

        // make sure we got the shopping items
        if (typeof shoppingItems !== 'object') {
            logger.logAlert("No shopping items provided!");
        }

        // initialization: make sure we have a firebase
        if (typeof firebase !== 'object') {
            logger.logAlert("No firebase access provided!");
        } else {
            // get basket content
            firebase.getBasketContent().then(function (data) {
                data = data || [];
                // get all properties
                Object.getOwnPropertyNames(data).forEach(function (prop) {
                    // check whether property really exists and is one containing data
                    if (data[prop] !== null && data[prop].item !== undefined) {
                        // add parsed object to items
                        basketItems.push({
                            item: parseInt(data[prop].item, 10),
                            amount: parseInt(data[prop].amount, 10)
                        });
                    }
                });
                // debug info about loaded basket content
                logger.logConsole("Loaded basket contents: " + JSON.stringify(basketItems));

                if (onDataLoaded !== undefined) {
                    onDataLoaded();
                }
            }).catch(function (err) {
                // notify the user of an error
                logger.logAlert("Error: " + err);
            });
        }

        this.getAmountInBasket = function (id) {
            var matchingItems = basketItems.filter(function (i) {
                return i.item === id;
            });

            return matchingItems.length > 0 ? matchingItems[0].amount : 0;
        };

        this.getBasketItems = function () {
            var items = [];

            basketItems.forEach(function (i) {
                items.push({
                    item: shoppingItems.filter(function (j) {
                        return j.id === i.item;
                    })[0],
                    amount: i.amount
                });
            });

            return items;
        };

        this.getTotalItemsCount = function () {
            var count = 0;

            basketItems.forEach(function (i) {
                count += i.amount;
            });

            return count;
        };

        this.getItemCount = function () {
            return basketItems.length;
        };

        this.getTotalCost = function () {
            var cost = 0;

            this.getBasketItems().forEach(function (i) {
                cost += i.amount * i.item.price;
            });

            return cost;
        };

        this.addItem = function (item, itemCount) {
            var i, matchingItems;

            // ensures a valid item was passed
            if (typeof item !== 'number') {
                logger.logAlert("No valid item passed!");
                return false;
            }

            // ensures a valid count was passed
            if (typeof itemCount !== 'number' || itemCount < 1) {
                logger.logAlert("No valid count passed!");
                return false;
            }

            matchingItems = basketItems.filter(function (i) {
                return i.item === item;
            });
            if (matchingItems.length > 0) {
                // item found, increase count
                matchingItems[0].amount += itemCount;
                firebase.setBasketContent(JSON.stringify(basketItems));
                return true;
            }

            // item not found, add new item to basket
            basketItems.push({
                item: item,
                amount: itemCount
            });

            firebase.setBasketContent(JSON.stringify(basketItems));
            return true;
        };

        this.removeItem = function (item, itemCount) {
            var i, matchingItems;

            // ensures a valid item was passed
            if (typeof item !== 'number') {
                logger.logAlert("No valid item passed!");
                return false;
            }

            // ensures a valid count was passed
            if (itemCount === null || itemCount === undefined || itemCount < 1) {
                logger.logAlert("No valid count passed!");
                return false;
            }

            // try and find item in items
            for (i in basketItems) {
                if (basketItems.hasOwnProperty(i)) {
                    if (basketItems[i].item === item) {
                        // item found, check count
                        if (basketItems[i].amount === itemCount) {
                            // remove item completely
                            basketItems.splice(i, 1);
                        } else if (basketItems[i].amount > itemCount) {
                            // reduce item count
                            basketItems[i].amount -= itemCount;
                        } else {
                            // ERROR!
                            logger.logAlert("Attempting to remove more instances of " + item.title + " than are in basket!");
                            return false;
                        }

                        firebase.setBasketContent(JSON.stringify(basketItems));
                        return true;
                    }
                }
            }

            // ERROR!
            logger.logAlert("Item removal unsuccssful because " + item.title + " was not found!");
            return false;
        };
    }

    // Export to window
    window.app = window.app || {};
    window.app.Basket = Basket;
}(window));