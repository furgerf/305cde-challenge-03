/*global app, alert*/

(function (window) {
    'use strict';

    function Item(title, description, price, id, logger) {
        this.title = title || "NO_TITLE";
        this.description = description || "NO_DESCRIPTION";

        if (logger === undefined || logger === null) {
            alert("NO LOGGER PASSED TO ITEM");
        }

        if (typeof id === "number") {
            this.id = id;
        } else if (typeof id === "string" && !isNaN(id)) {
            this.id = parseInt(id, 10);
        } else {
            if (logger !== undefined) {
                logger.logAlert("Invalid id: " + id || "NO_ID");
            }
        }

        if (typeof price === "number") {
            this.price = price;
        } else if (typeof price === "string" && !isNaN(price)) {
            this.price = parseFloat(price);
        } else {
            if (logger !== undefined) {
                logger.logAlert("Invalid price: " + price || "NO_PRICE");
            }
        }
    }

    // Export to window
    window.app = window.app || {};
    window.app.Item = Item;
}(window));